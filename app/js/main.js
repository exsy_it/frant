        var _active_i = 1;
        var _flag_3 = false;

(function() {
	'use strict';

(function ($) {
  $.fn.numberPicker = function() {
    var dis = 'disabled';
    return this.each(function() {
      var picker = $(this),
          p = picker.find('a:last-child'),
          m = picker.find('a:first-child'),
          input = picker.find('input'),                 
          min = parseInt(input.attr('min'), 10),
          max = parseInt(input.attr('max'), 10),
          inputFunc = function(picker) {
            var i = parseInt(input.val(), 10);
            if ( (i <= min) || (!i) ) {
              input.val(min);
              p.prop(dis, false);
              m.prop(dis, true);
            } else if (i >= max) {
              input.val(max);
              p.prop(dis, true); 
              m.prop(dis, false);
            } else {
              p.prop(dis, false);
              m.prop(dis, false);
            }
          },
          changeFunc = function(picker, qty) {
            var q = parseInt(qty, 10),
                i = parseInt(input.val(), 10);
            if ((i < max && (q > 0)) || (i > min && !(q > 0))) {
              input.val(i + q);
              inputFunc(picker);
            }
          };
      m.on('click', function(e){
        e.preventDefault();
        changeFunc(picker,-1);
    });
      p.on('click', function(e){
        e.preventDefault();
        changeFunc(picker,1);
    });
      input.on('change', function(){inputFunc(picker);});
      inputFunc(picker);
    });
  };
}(jQuery));

	/* Работа скриптов после загрузки документа */
	$(window).on("load", function() {

        $('.offer__count-counter').numberPicker();
        $('.steps__counter').numberPicker();
        $('.options__slider-counter').numberPicker();

// WHAT DO YOUR SCRIPT ?
    
    $(function() {
      $(".phone-input").mask("+7 (999) 999-99-99");
    });

    $(function() {
    $('.to-top').click(function(e) {
      e.preventDefault();
              var top = $('body').offset().top;
              $('body , html').animate({scrollTop: top}, 800);
            });

    });

		$(function() {
			$('.header__burger').click(function () {
				$(this).toggleClass('active');
				$('.site-menu ').toggleClass('show');
				$('.main .main-banner_slider .slick-dots').toggleClass('hide');
				$('body').toggleClass('overflow');
            });
		});

        $(function() {
            $('.drinks__catalog').hide();
            $('.drinks__catalog:first').show();
            $('.drinks__catalog.first').show();
            $('.drinks__link:first').addClass('active');
            $('.drinks__link.first').addClass('active');
            $('.drinks__link').click(function(e) {
            	e.preventDefault();
                $('.drinks__link').removeClass('active');
                $(this).addClass('active');
                $('.drinks__catalog').hide();
                var selectTab = $(this).attr("href");
                $(selectTab).fadeIn();
            });

            $('.drinks__mobile .drinks__link').click(function(e) {
              var top = $('.drinks__mobile').offset().top - 100;
              $('body , html').animate({scrollTop: top}, 800);
            });
        });


        $(function() {
            $('.type__tab-content').hide();
            $('.type__item').click(function(e) {
                e.preventDefault();
                $('.type__item').removeClass('active');
                $(this).addClass('active');
                $(this).closest($('.type__column')).next($('.type__column')).find($('.type__tab-content')).hide();
                var selectTab = $(this).find('a').attr("href");
                $(selectTab).show();
                $(this).closest('.type__column').addClass('mob-hide');
                $(this).closest('.type__column').next().removeClass('mob-hide');
                $(this).closest('.type__column').next().find($('.type__column-link')).fadeIn();

            });

            $('.type__column_one .type__item').click(function(e) {
              $('.type__column_three').find('.type__tab-content').hide();
            });

            $('.type__column-link').click(function(e) {
              $(this).closest('.type__column').prev().removeClass('mob-hide');
              $(this).fadeOut();
              $(this).closest('.type__column').addClass('mob-hide');
            });
        });

        $(function () {
          $('.type__row .radio input').change(function() {
            $('.type .steps__next').show();
          });
        });

        $(function () {
          $('.coctails-modal .drink-item__image').click(function() {
            $(this).parent().toggleClass('active');
            $(this).parent().siblings().removeClass('active');
            $(this).siblings().find($('.drink-item__popup-open')).removeClass('hide');
            $(this).siblings().find($('.drink-item__popup-list')).slideUp();
          });
        });

        $(function () {
          $('.drink-item__popup-open').click(function(e) {
            e.preventDefault();
            $(this).addClass('hide');
            $(this).siblings($('.drink-item__popup-list')).slideDown();
          });
        });

        $(function () {
            $('.main-banner_slider').slick({
                infinite: true,
                autoplay: true,
                slidesToShow: 1,
                slidesToScroll: 1,
				fade: true,
				dots: true
            });
        });

        $(function () {
          $('.bar-choosing__slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                arrows: true
            });
        });

        $(function () {
$('.catalog-modal__slider').slick({
        slidesToShow: 2,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        responsive: [
        {
          breakpoint: 767,
          settings: {
          slidesToShow: 1,
          slidesToScroll: 1
          } 
        }
        ]
    });
        });

        var cartCount = 1;
        $(function () {
          $('.drink-item__popup-add, .bar-choosing__button').click(function(e) {
            e.preventDefault();
            

            $('.header__cart span').text(cartCount++);
          });
        });

        $(function () {
          $('.js-fancybox').fancybox({
            arrows: true
          });

          $('.catalog-modal__item img').click(function(e) {
              e.preventDefault();
              $(this).siblings($('.js-fancybox')).click();
          });
        });

        $(function () {

          $('.overlay').click(function() {
            $('.modal').removeClass('show');
            $(this).fadeOut();
            $('body').removeClass('overflow');
          });

          $('.modal-close, .coctails-modal__back').click(function() {
            $('body').removeClass('overflow');
            $('.modal').removeClass('show');
            $('.overlay').fadeOut();
          });

        
          $('.js-catalog').click(function(e) {
            e.preventDefault();
            $('body').addClass('overflow');
            $('.catalog-modal').addClass('show');
            $('.overlay').fadeIn();
          });

          $('.header__button, .cards__button, .stock__link, .main-banner__info-link, .steps__quick').click(function(e) {
            e.preventDefault();
            $('body').addClass('overflow');
            $('.feedback-modal').addClass('show');
            $('.overlay').fadeIn();
          });



          $('.discount__link').click(function(e) {
            e.preventDefault();
            $('body').addClass('overflow');
            $('.callback-modal').addClass('show');
            $('.overlay').fadeIn();
          })

          $('.steps .results__bottom-row a').click(function(e) {
            e.preventDefault();
            $('body').addClass('overflow');
            $('.callback-modal').addClass('show');
            $('.overlay').fadeIn();
          });

          $('.drink-item__button').click(function(e) {
            e.preventDefault();
            $('body').addClass('overflow');
            $('.coctails-modal').addClass('show');
            $('.overlay').fadeIn();
          });

          $('.header__cart').click(function(e) {
            e.preventDefault();
            $('body').addClass('overflow');
            $('.cart-modal').addClass('show');
            $('.overlay').fadeIn();
          });           

          $('.contact-modal__button').click(function(e) {
            e.preventDefault();
            $(this).closest('.modal').removeClass('show');
            $('.thankyou-modal').addClass('show');
          });

          $('.results__submit').click(function(e) {
            e.preventDefault();
            $('body').addClass('overflow');
            $('.finish-modal').addClass('show');
            $('.overlay').fadeIn();
          });  
          
          $('.thankyou-modal .contact-modal__button').click(function() {
            $('body').removeClass('overflow');
            $('.modal').removeClass('show');
            $('.overlay').fadeOut();
          });

        });
        
        $(function() {
            $('.options__slider-image img').click(function(e) {
                $(this).parent().addClass('active');
            });

            $('.steps .options__slider-counter a').click(function(e) {
              e.preventDefault();
              
              var counterValue = $(this).siblings('input').val();   
              if (counterValue == 0) {
                $(this).parent().parent().removeClass('active'); 
                $(this).siblings('input').val(1);
              }
            });

            $('.steps .options__slider-counter input').change(function() {
              if ($(this).val() == 0) {
                $(this).parent().parent().removeClass('active'); 
                $(this).val(1);
              }
            });
        });

        $(function() {
            $('.results__services-close').click(function(e) {
                $(this).closest('.results__services-item').remove();
            });
        });
        
        $(function() {
            $('.results__bar-counters-close').click(function(e) {
                $(this).closest('.results__bar-counters-item').remove();
            });

             $('.cards__delete').click(function(e) {
                $(this).closest('.cards__column').remove();
            });
        });


        $(function() {
            $('.steps__item:first').addClass('active');
            $('.steps__name:first').addClass('active');
            stepHeight();

            $('.steps__name').click(function() {
              $(this).siblings().removeClass('active');
              $(this).addClass('active');
              var currentData = $(this).attr('data-i');
              $(".steps__item").removeClass('active');
              $(".steps__item[id='"+currentData+"']").addClass('active');
              _active_i = currentData;
              startSlider_3();
              stepHeight();
              stepToUp();
            });

            $('.steps__next-button').click(function(e) {
                e.preventDefault();

                $(".steps__name[data-i='"+_active_i+"']").removeClass("active");
                _active_i++;
                $(".steps__name[data-i='"+_active_i+"']").addClass("active");
                
                $(this).closest('.steps__item').removeClass('active');
                $(this).closest('.steps__item').next().addClass('active');
                startSlider_3();
                stepHeight();
                stepToUp();
            });

            $('.steps__back').click(function(e) {
                e.preventDefault();

                $(".steps__name[data-i='"+_active_i+"']").removeClass("active");
                _active_i--;
                $(".steps__name[data-i='"+_active_i+"']").addClass("active");

                $(this).closest('.steps__item').removeClass('active');
                $(this).closest('.steps__item').prev().addClass('active');
                startSlider_3();
                stepHeight();
                stepToUp();
            });

            $('.results__services-link').click(function(e) {
              e.preventDefault();
              $('.steps__name.active').removeClass('active');
              $('.steps__item.active').removeClass('active');
              $('.steps__item:first').addClass('active');
              $('.steps__name:first').addClass('active');
            });
        });

     $(function() {
        var bannerHeight = $('.main-banner').height();
      $(window).on( 'scroll', function() {
              if ($(window).scrollTop() > bannerHeight) {
                $('.to-top').addClass('show');
              } 
              else {
                $('.to-top').removeClass('show');
              }
      });
    });   

	});

	/* Работа скриптов после загрузки документа и ресайза */
	// $(window).on("resize", function() {
    
	// });
}());

function startSlider_3(){
  if(_active_i == 3 && !_flag_3){
    $('.options__slider').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                infinite: false,
                responsive: [
                {
                  breakpoint: 767,
                  settings: {
                  slidesToShow: 2
                  },
                  breakpoint: 576,
                  settings: {
                  slidesToShow: 1
                  }  
                } 
                ]
              });
    _flag_3 = true;
  }
}

function stepHeight() {
  var height = $('.steps__item.active').height() + 150;
  $('.steps__list').css('height', height);  
}

function stepToUp() {
  var stepTop = $('.steps__container').offset().top - 200;
            $('body , html').animate({scrollTop: stepTop}, 800);
}