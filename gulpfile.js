(function() {

	"use strict";

	var gulp = require('gulp'),
		rename       = require('gulp-rename'),
		notify       = require('gulp-notify'),
		prefix       = require('gulp-autoprefixer'),
		sass         = require('gulp-sass'),
		// minifyCSS = require('gulp-clean-css'),
		uglify    	 = require('gulp-uglify'), // (для сжатия JS)
		del          = require('del'),
		imagemin     = require('gulp-imagemin'),
		pngquant     = require('imagemin-pngquant'),
		cache        = require('gulp-cache'),
		browserSync  = require('browser-sync'),
		qcmq         = require('gulp-group-css-media-queries'),
		reload       = browserSync.reload;

	// Paths
	var paths = {
		app  : {
			html 		: './app/',
			img  		: './app/img/**/*.*',
			svg  		: './app/svg/',
			video 	: './app/video/**/*.*',
			css  		: './app/css/',
			scss 		: './app/scss/**/*',
			js   		: './app/js/**/*.js',
			fonts 	: './app/fonts/**/*.*',
    	favicon :'./app/favicon/**/*'
		},
		build: {
			html 		: 'dist/',
			img  		: 'dist/img/',
			svg  		: 'dist/svg/',
			video 	: 'dist/video/',
			css  		: 'dist/css/',
			js   		: 'dist/js/',
			fonts 	: 'dist/fonts/',
    	favicon : 'dist/favicon/'
		},
		clean: './dist'
	};

	// Browser-Sync
	gulp.task('browserSync', function() {
		browserSync({
			server: {
				baseDir: paths.app.html
			},
			open  : true,
			notify: false
		});
	});

	// sass
	gulp.task('sass', function() {
		gulp.src(paths.app.scss)
		.pipe(sass().on('error', notify.onError({
			message: "<%= error.message %>",
			title  : "Sass ERROR!"
		})))
		.pipe(prefix( ['last 15 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']))
		.pipe(qcmq())
		.pipe(gulp.dest(paths.app.css))
		.pipe(reload({
			stream: true
		}));
	});

	/* html */
	gulp.task('html', function() {
		gulp.src(paths.app.html + '*.html')
		.pipe(reload({
			stream: true
		}));
	});

	// js
	gulp.task('js', function() {
		return gulp.src(paths.app.js)
		.pipe(reload({ 
			stream: true
		}));
	});

	// "img"
	gulp.task('img', function() {
		return gulp.src(paths.app.img)
		.pipe(cache(imagemin({
			interlaced : true,
			progressive: true,
			svgoPlugins: [{
				removeViewBox: false
			}],
			use        : [pngquant()]
		})))
		.pipe(gulp.dest(paths.build.img));
	});

	// "video"
	gulp.task('video', function(){
		gulp.src(paths.app.video)
		.pipe(gulp.dest(paths.app))
		.pipe(browserSync.reload({stream: true}))
	});

	// svg old
	gulp.task('svg', function () {
	gulp.src(paths.app.svg)
		.pipe(gulp.dest(paths.build.svg));
	});

	// fonts
	gulp.task('fonts', function () {
		return gulp.src(paths.app.fonts)
		.pipe(gulp.dest(paths.build.fonts))
	});

	// favicon
	gulp.task('favicon', function () {
		return gulp.src(paths.app.favicon)
		.pipe(gulp.dest(paths.build.favicon))
	});

	// watch
	gulp.task('watch', ['browserSync', 'sass'], function(callback) {
		gulp.watch(paths.app.scss + '**/*.*', ['sass']);
		gulp.watch(paths.app.html + '*.html', ['html']);
		gulp.watch(paths.app.js, ['js']);
		gulp.watch(paths.app.img, ['img']);
		gulp.watch(paths.app.svg, ['svg']);
	});

	// defaultPrevented
	gulp.task('default', ['watch']);

	// clean - delete 'dist' before build
	gulp.task('clean', function() {
		return del(paths.clean);
	});

	//  "build"
	gulp.task('build', ['clean', 'img'], function(callback) {

		gulp.src(paths.app.html + '**/*.html')
		.pipe(gulp.dest(paths.build.html));

		gulp.src(paths.app.css + '**/*.css')
		.pipe(gulp.dest(paths.build.css));

		gulp.src(paths.app.js)
		.pipe(uglify()) // min.js
  	.pipe(gulp.dest(paths.build.js));

		gulp.src(paths.app.video)
		.pipe(gulp.dest(paths.build.video));

		gulp.src(paths.app.fonts)
		.pipe(gulp.dest(paths.build.fonts));

		gulp.src(paths.app.svg)
		.pipe(gulp.dest(paths.build.svg));

		gulp.src(paths.app.favicon)
		.pipe(gulp.dest(paths.build.favicon));

	});

	//  "clear" - clean cache for tast 'img'
	gulp.task('clear', function() {
		return cache.clearAll();
	});

})();
